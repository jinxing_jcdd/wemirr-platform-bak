package com.wemirr.platform.tools.mapper;

import com.wemirr.framework.boot.SuperMapper;
import com.wemirr.platform.tools.domain.entity.DynamicReleaseDrag;
import org.springframework.stereotype.Repository;

/**
 * @author Levin
 */
@Repository
public interface DynamicReleaseDragMapper extends SuperMapper<DynamicReleaseDrag> {
}
